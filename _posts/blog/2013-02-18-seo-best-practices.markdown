---
layout: post
title:  "Best Practices for Search Engine Optimization"
date:   2013-02-18 12:48:54 -0500
categories:
- blog
---

### Keywords
* Any words that people search will show up in search results bolded - this includes words in a URL, Titles, Descriptions, or whatever snippet displayed in the result.
* Targeting a long tail keyword offers less competition for rankings (eg. "SEO Tips" vs. "SEO Tips 2013" - where the latter is the long tail keyword)

### Title Tags
* Page titles are displayed in search results 
* Titles should effectively communicate the topic of the page content with relevant keywords
* Each page should have a different, more specific, title - Avoid using a single title tag across all pages
* Avoid extremely long titles and key word stuffing 

### Description Meta Tags
* Google will use either the meta description or a snippet from the page in it's results depening on what best matches a users search 
* Use unique descriptions for each page
* Use this tag to summarize what the page is about using appropriate keywords

### URL Structure
* URLs will also show up in search results, so easy to understand URLs are helpful
* Use descriptive file and folder names for organization and better crawling of documents by search enginges
* Use words that are relevant to page content in URLs
* Have a simple, organized directory structure

### Navigation
* Make the navigation easy for users and crawlers to go from general content to more specific content
* Organized HTML Site Maps displayed on your site for important pages will help users to better navigate
* XML Sitemap files help ensure that search engines discover all the necessary pages of your site
* Create custom 404 pages that link back to the root page or lists related content

### Links
* Use appropriate anchor text so search engines can easily understand what linked pages contain
* Add `rel="nofollow"` to links that you don't trust/don't want to pass reputation to
* Linking to sites that Google considers to be "spammy" can affect the reputation and ranking of your site
* Integrate internal links throughout your site to boost traffic to individual pages

### Images
* Provide information about images in the alt text to help screen readers as well as search engines to better understand the image content
* Optimize file names to allow image search engines to better understand the image
* Consolidate images into a single directory
* Image sitemaps can provide google with more information about the images found on your site

### Robots.txt
* Effectively use this file to tell search engines whether or not they can access and crawl specifice parts of a site
* Don't rely on this file to block sensitive or confidential material

### Mobile Sites
* Create a mobile sitemap and submit it to Google just like a standard sitemap to ensure that Google knows of the site's existence
* If the site is restricted to mobile phones only, make sure Google can access it by checking whether the User-Agent contains the string "Googlebot-Mobile"

### Social Media
* Social media sites are used as a part of search engine rankings for Google and Bing. Active promotion of sites on Twitter, Facebook, etc will help to increase rank

### Related Resources
* Google's [Webmaster Tools](https://www.google.com/webmasters/tools/) help to better control how Google interacts with sites
* Google's [SEO Optimization Starter Guide](http://static.googleusercontent.com/external_content/untrusted_dlcp/www.google.com/en//webmasters/docs/search-engine-optimization-starter-guide.pdf) contains more detailed information about the above best practices and more
