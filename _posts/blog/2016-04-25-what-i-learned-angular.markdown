---
layout: post
title:  "What I learned while trying to learn Angular"
date:   2016-04-25 12:48:54 -0500
categories:
- blog
---

In the world of Front End, imposter syndrome is incessant. To be head first in a profession where the tools of the trade change, what feels like, monthly, how are you ever supposed to feel like you really know what you're doing? It's tough.

I graduated from a 2-year college program called Multimedia Design and Production. It covered a lot of different things, and I think the name has even changed now. I still remember, as we were nearing the end of the 2 years and we're supposed to be applying for jobs, telling a professor that I just didn't know what I really knew... you know? His advice to me was to look at job postings all the time, even if I'm not looking for a job. It helps to guage what the industry is doing and using, and evaluate my skills against that. So I did... and I still do. 

Which led me to Angular... you didn't even need to look at a job post to see it happen - the explosive popularity of the framework. But as I did look for a new job, I saw it everywhere. Because, as I said, tools change all the time, sometimes it's hard to know what ones are really here to stay. I went to the docs first, and followed along the tutorial. I tried to veer off course and make my app different enough from the tutorial that I would be forced to go out of my way for answers. It was fun. It was cool. It was awesome to see how easy it was to build a search functionality where the results just filtered as you typed. I mean, it took like 2 minutes! 

I built another app, similar in complexity (read: not complex), just to reiterate what I had learned. Cool. I remembered some stuff. It was relatively easy, not necessarily challenging. 

I didn't *learn* Angular though... like really learn it. I couldn't start an Angular project from scratch, I didn't **understand** it, fundamentally. But it wasn't the tutorials fault, or that I wasn't paying attention. What I did learn, was that I had a lot to learn.

My education was a high level of a lot of things: photo manipulation, flash, web design, video effects, 3d animation, web development. It was fun, I loved every project and I definitely learned a lot. My professors even made a point not to teach us jQuery, and only Javascript. Awesome. But it wasn't programming. I didn't learn those fundamental programming concepts that, I assume, make it just a little easier to jump into different languages. 

My first job taught me how to work in a Rails app, how to use git, and the terminal. I learned a lot about CSS and Sass in-depth. I was formatting HTML (HAML, specifically) around an app that was set up by a backend developer, and styling it using Sass. I wrote some simple jQuery here and there but for the most part, the complex Javascript was handled by the back end developers, too. I never really had to flex my Javascript muscles or learn something on a real project.

I learned that I needed to go back to basics. I know how to write Javascript, but I don't know how to write *good* Javascript; what makes it good. Don't get me wrong, I can look at some functions and see that they're less than ideal. But I'm certainly not confident enough to tell someone they should change it. 

## I learned that I needed to stop learning Angular, and learn Javascript. 

My imposter syndrome manifests itself in random attacks of feeling like I need to learn a million things all at once so I can be just like that really cool person I follow on Twitter that works at Google. I want to master the Canvas with awesome interactive animations. I want to build fast web apps. I want it all. But this just leads to failure before I even start, so much stress about being overwhelmed that I give up without really trying. 

But now I know where to start now: Javascript. 

Plain and simple. 

If you're feeling like I'm feeling - [this is where I started](https://www.udemy.com/understand-javascript/). I'm hoping to post more about my journey with Javascript. And I'd like to get more active on Codepen as I continue to learn.

I wrote this post for accountability and to remember where I came from.
