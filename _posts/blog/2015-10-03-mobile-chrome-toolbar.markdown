---
layout: post
title:  "Setting the mobile Chrome toolbar colour"
date:   2015-10-03 12:48:54 -0500
categories:
- blog
---

Ever browsed a website on your phone (in Chrome!) and noticed that the toolbar changed colour? If you've never noticed this, or never knew how it was done, it's actually so simple. 
All you have to do is add the following meta tag, with whatever colour you want. 


{% highlight html %}
<meta name="theme-color" content="#db5945">
{% endhighlight %}

Thank Google!

[Read more here](https://developers.google.com/web/updates/2014/11/Support-for-theme-color-in-Chrome-39-for-Android)