---
layout: feature
title:  "GIF Gallery"
date:   2016-03-11 12:48:54 -0500
categories:
- features
tags: "gif, gallery, css"
img: "/assets/thumbs/th-dev-stella-1.jpg"
---

The Stella Artois GIF gallery was just one of the GIF applications I worked on during my time with *Mosaic*. The entire experience features a GIF capture application, that I develop the styles for on top of a Cordova and Phonegap back end, which then posts to an online gallery. This gallery lazy loads all of the GIFs using [jQuery InView](https://remysharp.com/2009/01/26/element-in-view-event-plugin) and allows users to endlessly scroll through all of the gifs and view each one in a modal.

[View the live site here](http://gifs.stellaartois.com/verify_lda)

![Focus on the Facts website](/assets/feature/dev-stella-1.jpg)
![Focus on the Facts website](/assets/feature/dev-stella-2.jpg)
![Focus on the Facts website](/assets/feature/dev-m-stella-1.jpg)