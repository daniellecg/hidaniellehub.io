---
layout: feature
title:  "Creative Spirit Animal"
date:   2016-03-16 12:48:54 -0500
categories:
- features
tags: "jquery, css3, keyframe animation"
img: "/assets/thumbs/th-dev-confetti.jpg"
---

A personality quiz to find out what your Creative Spirit Animal is. This project was built during my time with *Mosaic* as an internal project. With about a week of development time, the site is fully responsive and built to work in all modern browsers. The quiz logic is written in jQuery and all of the animations are done using CSS Keyframe Animations. I'm extremely proud of this project, being my first time using CSS animation in-depth, and I think the end product is adorable.

Find out your creative spirt animal, [here](/creative-animal/)
(**Note**: this is a refactored version for demonstration purposes only)

![Focus on the Facts website](/assets/feature/dev-confetti-mobile.jpg)
![Focus on the Facts website](/assets/feature/dev-confetti.jpg)
![Focus on the Facts website](/assets/feature/dev-confetti2.jpg)