---
layout: feature
title:  "End Diabetes"
date:   2017-05-10 12:48:54 -0500
categories:
- features
tags: "Kentico, Vimeo, animation"
img: "/assets/thumbs/dc-thumb.jpg"
---

Paired with a rebrand, Diabetes Canada wanted to launch a movement microsite that challenged the perception of Diabetes across Canada and drove people to participate and donate. End Diabetes was that. I worked closely with our internal strategy/design team as well as various client stakeholders to brainstorm and put together a microsite that could be used throughout various campaigns. The design was impactful and told a story, it includes custom CSS animations that trigger on scroll - including a particularily tricky sticky progress bar on the home page that strikes out content while the user scrolls the page (that is also editable through the CMS!) - as well as a number of off-grid design elements. 

This site uses a fully flexbox grid, jQuery Waypoints and an embedded Vimeo player with a custom modal solution. The Impact page features a user generated content wall via [Sparkle](https://getsparkle.io/), using a custom solution that swaps the Sparkle theme between mobile and desktop breakpoints to achieve more design consistency. The site is built within Kentico CMS and is extremely flexible.


[Join the movement to End Diabetes here](http://enddiabetes.ca/)

![End Diabetes Homepage](/assets/feature/dc-desktop-1.jpg)
![End Diabetes Mobile screens](/assets/feature/dc-mobile.jpg)
![End Diabetes Story section](/assets/feature/dc-desktop-2.jpg)
![End Diabetes form](/assets/feature/dc-desktop-4.jpg)
![End Diabetes Impact page](/assets/feature/dc-desktop-3.jpg)