---
layout: feature
title:  "ecentric-elves"
date:   2017-12-20 12:48:54 -0500
categories:
- features
tags: "Vuejs, mentorship, pair programming"
img: "/assets/thumbs/elves-t-min.jpg"
---

I had the honour of working on the ecentricarts holiday card for the second year in a row. This years holiday card was an elf generator, featuring some of the employees at ecentricarts. As an internal project, the team got together to brainstorm and sketch out possible ideas before landing on this one. Part of what we loved about the final idea, ecentric-elves, was that not only are the elves adorable looking, we were able to make the experience fun for visually impaired users as well, by including quirky descriptions for our elves instead of boring alt text.

The card was built entirely using Vue, my third time using it now, and was extremely easy to build as I was able to utilize a lot of my learnings from the year before (my first holiday card, and first time using Vue). One of the challenges on this project, was implementing the design in a responsive way and ensuring our elves always appeared to be standing on the snow. The snow hills move up and down with the screen, and we wanted to avoid scroll bars as much as possible, as the design appeared to be more like an app than a website. I attempted to style the elves using viewport units, to make full responsive elves... but unfortunately Safari flexbox bugs killed those dreams, and we reverted at the last minute to multiple custom breakpoints.

This year, I worked closely with a junior developer on the project, introducing her to Vue and facilitating code reviews. Each development day, we sat down together and scoped out the work we would do that day and took turns entering the code. Overall it was a really great experience and the end result is pretty fun!


![ecentric-elves Homepage](/assets/feature/elves1-min.jpg)
![ecentric-elves mobile screens](/assets/feature/elvesm-min.jpg)
![ecentric-elves form page](/assets/feature/elves2-min.jpg)
![ecentric-elves elf page](/assets/feature/elves3-min.jpg)
