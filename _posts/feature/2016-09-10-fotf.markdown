---
layout: feature
title:  "Focus on the Facts"
date:   2016-09-28 12:48:53 -0500
categories:
- features
tags: "accessibility, css, jquery, government"
img: "/assets/thumbs/th-dev-fotf-1.jpg"
---

A respsonsive quiz microsite to help users learn how to read Nutrition Facts tables. Audited for *WCAG level AA* compliance. Had to be fully **keyboard accessible** as well. Nutrition Facts tables were built as live text using **Fittext** to help maintain proportions to look like a real Nutrition Facts table. 

<!-- [View the site](http://focusonthefacts.ca) -->
As this is a contest, the site is only live once a year.

Code samples available upon request.

![Focus on the Facts website](/assets/feature/dev-fotf-1.jpg)
![Focus on the Facts website](/assets/feature/dev-fotf-2.jpg)
![Focus on the Facts website](/assets/feature/dev-fotf-m.jpg)
