---
layout: feature
title:  "Heineken MapBox"
date:   2016-03-14 12:48:54 -0500
categories:
- features
tags: "heineken, mapbox, parallax, jade"
img: "/assets/thumbs/th-hkn.jpg"
---

These two simple campaign websites for Heineken, Embrace the Cold and BrewLock, were built during my time with *Sandbox Advertising Inc.*. Using my [Grunt Jade Boilerplate](https://github.com/hidanielle/grunt-boilerplate-jade) these sites featured a [MapBox](https://www.mapbox.com/) map to locate events across Canada. Brewlock uses [Fittext.js](http://fittextjs.com/) on the main header so that it displays beautifully across screen sizes, as well as [scrollReveal](https://scrollrevealjs.org/) and [Stellar.js](http://markdalgleish.com/projects/stellar.js/) to bring the page to life with subtle parallax and reveal animations. 

Map functionality on the site includes filtering across city as well as the different types of events. Embrace the Cold also hooked up to the Open Weather API to pull in weather information for whatever city you select.

Unfortunately, as this was a campaign website, the site is no longer active.

![Heineken website](/assets/feature/dev-hkn-1.jpg)
![Heineken website](/assets/feature/dev-hkn-2.jpg)
![Heineken website](/assets/feature/hkn-3.jpg)
![Heineken website](/assets/feature/dev-hkn-m.jpg)