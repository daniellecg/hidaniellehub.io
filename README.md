## hidanielle

### To Do:
* Optimize image sizes (right now its really bad...)
* Edit old blog posts
* Adjust text highlight colour on orange text (its illegible)
* Speed tests
* Make fully accessible
* Add animations
* Add smooth scrolling
* Load more on blog
* Add site map
* Tweak style of blog
